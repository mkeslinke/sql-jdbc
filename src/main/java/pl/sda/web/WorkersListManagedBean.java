package pl.sda.web;


import pl.sda.dao.IWorkersDAO;
import pl.sda.dao.WorkersDAO;
import pl.sda.dao.WorkersDAOJDBCimpl;
import pl.sda.dao.WorkersDaoHibernateImpl;
import pl.sda.dto.Worker;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;

@SessionScoped
@ManagedBean(name = "workerList")
public class WorkersListManagedBean {

    private Worker newWorker = new Worker();
//  private IWorkersDAO workersDAO = new WorkersDAOJDBCimpl();
    private IWorkersDAO workersDAO = new WorkersDaoHibernateImpl();


    public List<Worker> getList() {
        return workersDAO.getAllWorkes();
    }

    public void addNewWorker() {
        // dodać pracownika do bazy

        workersDAO.saveWorker(newWorker);

        newWorker = new Worker();

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Nowy pracownik został dodany!"));
    }



    public void deleteWorker(int id) {
        // dodać pracownika do bazy
        workersDAO.deleteWorker(id);

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Usunięto pracownika z id: " + id));
    }

    public void editWorker(int id){
        newWorker = workersDAO.getWorker(id);
    }

    public void updateWorker(){
        workersDAO.updateWorker(newWorker);
        newWorker = new Worker();
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Pracownik został edytowany!"));
    }


    public Worker getNewWorker() {
        return newWorker;
    }


}
