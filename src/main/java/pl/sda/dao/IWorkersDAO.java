package pl.sda.dao;

import pl.sda.dto.Worker;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public interface IWorkersDAO {

    List<Worker> getAllWorkes();

    Worker getWorker(int workerId);

    void deleteWorker(int workerId);

    void saveWorker(Worker worker);

    void updateWorker(Worker worker);


}
